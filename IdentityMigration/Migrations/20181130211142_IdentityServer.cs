﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IdentityMigration.Migrations
{
    public partial class IdentityServer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "fields",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false),
                    field_name = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    field_key_name = table.Column<string>(unicode: false, maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_fields", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "groups",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false),
                    group_name = table.Column<string>(unicode: false, maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_groups", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "roles",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false),
                    role = table.Column<string>(unicode: false, maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_roles", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "users",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false),
                    login_id = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    password = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    first_name = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    middle_name = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    last_name = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    individual_party_id = table.Column<int>(nullable: true),
                    created_by_user_id = table.Column<int>(nullable: false),
                    created_at = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())"),
                    updated_by_user_id = table.Column<int>(nullable: true),
                    update_at = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_users", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "group_field_relationship",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false),
                    group_id = table.Column<int>(nullable: false),
                    field_id = table.Column<int>(nullable: false),
                    permissions = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    created_by_user_id = table.Column<int>(nullable: false),
                    created_at = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())"),
                    updated_by_user_id = table.Column<int>(nullable: false),
                    updated_at = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_group_field_relationship", x => x.id);
                    table.ForeignKey(
                        name: "FK_group_field_relationship_fields",
                        column: x => x.field_id,
                        principalTable: "fields",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_group_field_relationship_groups",
                        column: x => x.group_id,
                        principalTable: "groups",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "group_field_relationship_party_exceptions",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false),
                    party_id = table.Column<int>(nullable: false),
                    group_id = table.Column<int>(nullable: false),
                    field_id = table.Column<int>(nullable: false),
                    permissions = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    created_by_user_id = table.Column<int>(nullable: false),
                    created_at = table.Column<DateTime>(type: "datetime", nullable: false),
                    updated_by_user_id = table.Column<int>(nullable: false),
                    updated_at = table.Column<DateTime>(type: "datetime", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_group_field_relationship_party_exceptions", x => x.id);
                    table.ForeignKey(
                        name: "FK_group_field_relationship_party_exceptions_fields",
                        column: x => x.field_id,
                        principalTable: "fields",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_group_field_relationship_party_exceptions_groups",
                        column: x => x.group_id,
                        principalTable: "groups",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "role_group_relationship",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false),
                    role_id = table.Column<int>(nullable: false),
                    group_id = table.Column<int>(nullable: false),
                    priority = table.Column<int>(nullable: false),
                    created_by_user_id = table.Column<int>(nullable: false),
                    created_at = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())"),
                    updated_by_user_id = table.Column<int>(nullable: false),
                    updated_at = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_role_group_relationship", x => x.id);
                    table.ForeignKey(
                        name: "FK_role_group_relationship_groups",
                        column: x => x.group_id,
                        principalTable: "groups",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_role_group_relationship_roles",
                        column: x => x.role_id,
                        principalTable: "roles",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "group_field_relationship_user_exceptions",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false),
                    party_id = table.Column<int>(nullable: false),
                    user_id = table.Column<int>(nullable: false),
                    group_id = table.Column<int>(nullable: false),
                    field_id = table.Column<int>(nullable: false),
                    permissions = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    created_by_user_id = table.Column<int>(nullable: false),
                    created_at = table.Column<DateTime>(type: "datetime", nullable: false),
                    updated_by_user_id = table.Column<int>(nullable: false),
                    updated_at = table.Column<DateTime>(type: "datetime", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_group_field_relationship_user_exceptions", x => x.id);
                    table.ForeignKey(
                        name: "FK_group_field_relationship_user_exceptions_fields",
                        column: x => x.field_id,
                        principalTable: "fields",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_group_field_relationship_user_exceptions_groups",
                        column: x => x.group_id,
                        principalTable: "groups",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_group_field_relationship_user_exceptions_users",
                        column: x => x.user_id,
                        principalTable: "users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "user_party_relationship",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false),
                    user_id = table.Column<int>(nullable: false),
                    party_id = table.Column<int>(nullable: false),
                    role_id = table.Column<int>(nullable: false),
                    created_by_user_id = table.Column<int>(nullable: false),
                    created_at = table.Column<DateTime>(type: "datetime", nullable: false),
                    updated_by_user_id = table.Column<int>(nullable: true),
                    updated_at = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_user_party_relationship", x => x.id);
                    table.ForeignKey(
                        name: "FK_user_party_relationship_roles",
                        column: x => x.role_id,
                        principalTable: "roles",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_user_party_relationship_users",
                        column: x => x.user_id,
                        principalTable: "users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_group_field_relationship_field_id",
                table: "group_field_relationship",
                column: "field_id");

            migrationBuilder.CreateIndex(
                name: "IX_group_field_relationship_group_id",
                table: "group_field_relationship",
                column: "group_id");

            migrationBuilder.CreateIndex(
                name: "IX_group_field_relationship_party_exceptions_field_id",
                table: "group_field_relationship_party_exceptions",
                column: "field_id");

            migrationBuilder.CreateIndex(
                name: "IX_group_field_relationship_party_exceptions_group_id",
                table: "group_field_relationship_party_exceptions",
                column: "group_id");

            migrationBuilder.CreateIndex(
                name: "IX_group_field_relationship_user_exceptions_field_id",
                table: "group_field_relationship_user_exceptions",
                column: "field_id");

            migrationBuilder.CreateIndex(
                name: "IX_group_field_relationship_user_exceptions_group_id",
                table: "group_field_relationship_user_exceptions",
                column: "group_id");

            migrationBuilder.CreateIndex(
                name: "IX_group_field_relationship_user_exceptions_user_id",
                table: "group_field_relationship_user_exceptions",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_role_group_relationship_group_id",
                table: "role_group_relationship",
                column: "group_id");

            migrationBuilder.CreateIndex(
                name: "IX_role_group_relationship_role_id",
                table: "role_group_relationship",
                column: "role_id");

            migrationBuilder.CreateIndex(
                name: "IX_user_party_relationship_role_id",
                table: "user_party_relationship",
                column: "role_id");

            migrationBuilder.CreateIndex(
                name: "IX_user_party_relationship_user_id",
                table: "user_party_relationship",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_users",
                table: "users",
                column: "login_id",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "group_field_relationship");

            migrationBuilder.DropTable(
                name: "group_field_relationship_party_exceptions");

            migrationBuilder.DropTable(
                name: "group_field_relationship_user_exceptions");

            migrationBuilder.DropTable(
                name: "role_group_relationship");

            migrationBuilder.DropTable(
                name: "user_party_relationship");

            migrationBuilder.DropTable(
                name: "fields");

            migrationBuilder.DropTable(
                name: "groups");

            migrationBuilder.DropTable(
                name: "roles");

            migrationBuilder.DropTable(
                name: "users");
        }
    }
}
