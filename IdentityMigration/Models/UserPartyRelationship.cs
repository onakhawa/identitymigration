﻿using System;
using System.Collections.Generic;

namespace IdentityMigration.Models
{
    public partial class UserPartyRelationship
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int PartyId { get; set; }
        public int RoleId { get; set; }
        public int CreatedByUserId { get; set; }
        public DateTime CreatedAt { get; set; }
        public int? UpdatedByUserId { get; set; }
        public DateTime? UpdatedAt { get; set; }

        public Roles Role { get; set; }
        public Users User { get; set; }
    }
}
