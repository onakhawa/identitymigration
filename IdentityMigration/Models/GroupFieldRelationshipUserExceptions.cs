﻿using System;
using System.Collections.Generic;

namespace IdentityMigration.Models
{
    public partial class GroupFieldRelationshipUserExceptions
    {
        public int Id { get; set; }
        public int PartyId { get; set; }
        public int UserId { get; set; }
        public int GroupId { get; set; }
        public int FieldId { get; set; }
        public string Permissions { get; set; }
        public int CreatedByUserId { get; set; }
        public DateTime CreatedAt { get; set; }
        public int UpdatedByUserId { get; set; }
        public DateTime UpdatedAt { get; set; }

        public Fields Field { get; set; }
        public Groups Group { get; set; }
        public Users User { get; set; }
    }
}
