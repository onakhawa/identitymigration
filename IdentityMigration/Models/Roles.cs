﻿using System;
using System.Collections.Generic;

namespace IdentityMigration.Models
{
    public partial class Roles
    {
        public Roles()
        {
            RoleGroupRelationship = new HashSet<RoleGroupRelationship>();
            UserPartyRelationship = new HashSet<UserPartyRelationship>();
        }

        public int Id { get; set; }
        public string Role { get; set; }

        public ICollection<RoleGroupRelationship> RoleGroupRelationship { get; set; }
        public ICollection<UserPartyRelationship> UserPartyRelationship { get; set; }
    }
}
